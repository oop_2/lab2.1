public class JavaVariables11 {
    public static void main(String[] args) {
        //Good
        int minutesPerHour = 60;

        //OK, but not so easy to understand what m acrually is
        int m = 60;
    }
}
